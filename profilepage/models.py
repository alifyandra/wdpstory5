from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class ClassYear(models.Model):
    year = models.CharField(primary_key=True, max_length=13)
    class_name = models.CharField(max_length=12)
    def __str__(self):
        return f"{self.year}"

class Friend(models.Model):
    name = models.CharField(primary_key=True, max_length=30)
    hobby = models.CharField(max_length=17)
    favourite_FnB = models.CharField(max_length=17)
    class_year = models.ForeignKey(ClassYear, on_delete=models.CASCADE)
    def __str__(self):
        return f"{self.name}"