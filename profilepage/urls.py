from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.home, name='profile-home'),
    path('bio/', views.bio, name='profile-bio'),
    path('contact/', views.contact, name='profile-contact'),
    path('education/', views.education, name='profile-education'),
    path('experience/', views.experience, name='profile-experience'),
    path('skills/', views.skills, name='profile-skills'),
    path('friendform/', views.friend_new, name='profile-friendform'),
    path('friendlist/', views.friend_list, name='profile-friendlist'),
]