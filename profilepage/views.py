from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from .models import *
from .forms import *
# Create your views here.
def home(request):
    return render(request, 'profilepage/index.html', {'title': "Alif's Homepage"})

def bio(request):
    return render(request, 'profilepage/biography.html', {'title': "Biography"})

def contact(request):
    return render(request, 'profilepage/contact.html', {'title': 'Contact'})

def education(request):
    return render(request, 'profilepage/education.html', {'title': 'Education'})

def experience(request):
    return render(request, 'profilepage/experience.html', {'title': 'Experience'})

def skills(request):
    return render(request, 'profilepage/skills.html', {'title': 'Skills'})

def friend_new(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save() 
    else:
        form = FriendForm()
    return render(request, 'profilepage/friendform.html', {'title': 'Who are you?','form': form})

def friend_list(request):
    form = Friend.objects.all()
    context = {
        'title': 'Friend List',
        'friend': form
    }
    return render(request, 'profilepage/friendlist.html', context)
