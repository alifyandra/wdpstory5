from django import forms
from .models import Friend, ClassYear

# class POSTClassclass_year(forms.ModelForm):
#     class Meta:
#         model = Classclass_year
#         fields = (
#             'class_year',
#             'class_name',
#         )

# class POSTFriend(forms.ModelForm):
#     class Meta:
#         model = Friend
#         fields = (
#             'name',
#             'hobby',
#             'favourite_FnB',
#         )

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = (
            'name',
            'hobby',
            'favourite_FnB',
            'class_year',
        )

    # only need to define `new_class_year`, other fields come automatically from model
    new_class_year = forms.CharField(max_length=30, required=False, label = "New Class Year")
    new_class_name = forms.CharField(max_length=12, required=False)

    def __init__(self, *args, **kwargs):
        super(FriendForm, self).__init__(*args, **kwargs)
        # make `class_year` not required, we'll check for one of `class_year` or `new_class_year` in the `clean` method
        self.fields['class_year'].required = False

    def clean(self):
        class_year = self.cleaned_data.get('class_year')
        new_class_year = self.cleaned_data.get('new_class_year')
        new_class_name = self.cleaned_data.get('new_class_name')
        if not class_year and not new_class_year and not new_class_name:
            # neither was specified so raise an error to user
            raise forms.ValidationError('Must specify either class_year or New class_year!')
        elif not class_year:
            # get/create `class_year` from `new_class_year` and use it for `class_year` field
            class_year, created = ClassYear.objects.get_or_create(year=new_class_year, class_name=new_class_name)
            self.cleaned_data['class_year'] = class_year

        return super(FriendForm, self).clean()